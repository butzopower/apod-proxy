# Implementation using `apigee-access`

This is an implementation of the APOD proxy built using the Apigee provided `apigee-access` library.

This represents an existing codebase we wish to migrate from.

## Externalized configuration

This tries to use `apigee-access`'s `getKeyValueMap` function to provide a key-value-mapping object that allows configuration to be read from Apigee Edge.  The KVM code will not work in any environment outside of Apigee Edge, as it relies on being able to require proprietary node code only available within the embedded node environment.

## Caching

This tries to use `apigee-access`'s `getCache` function to provide a cache object that allows puts and gets on the cache inside Apigee Edge.

## Testing

No tests for this one.
