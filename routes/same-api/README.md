# Implementation using collaborator that looks similar to `apigee-access`

This is an implementation of the APOD proxy built using a light-weight wrapper for talking to the platform that implements a very similar API to the one provided by `apigee-access`.

## Externalized configuration

Externalized configuration is accessed via our own `platform`'s `getKeyValueMap` function, which mimics the function provided by `apigee-access`. It uses a bound cred-hub service broker instance (or local environment variables). It allows the same arguments as `apigee-access`'s `getKeyValueMap`, though they are completely ignored.

## Caching

The cache can be via the `platform` object's `getCache` function, which mimics the function provided by `apigee-access`.  The returned cache object is a light-weight wrapper around the redis client, whose configuration is provided by a bound redis service (or local environment variables).

## Testing

Testing is done using `supertest` for making requests, `nock` for stubbing the NASA APOD API, and `mockery` for providing a fake version of the `platform` collaborator during test.

The `platform` mock is slightly more complicated than other implementations due to it needing to mimic a more traditional node callback API like that implemented by `apigee-access`.
