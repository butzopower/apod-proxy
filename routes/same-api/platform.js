const services = isCloud() ?
  require('../../services.cloud') :
  require('../../services.local');

const redis = require('redis');

const redisClient = redis.createClient(services.REDIS_OPTIONS);

const keyValueMap = {
  get(key, callback) {
    const value = services.credentials[key];
    callback(null, value);
  }
};

const cache = {
  put(key, value, ttl) {
    redisClient.setex(key, ttl, value);
  },

  get(key, callback) {
    redisClient.get(key, callback);
  }
};

const platform = {
  getCache(_name, _options) {
    return cache;
  },

  getKeyValueMap(_name, _scope) {
    return keyValueMap;
  }
};

function isCloud() {
  return process.env.VCAP_SERVICES !== undefined;
}

module.exports = platform;
