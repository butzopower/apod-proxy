const express = require('express');
const client = require('../../services/nasa.client');
const platform = require('./platform');
const router = express.Router();

function extractDate(date) {
  if (date === undefined) {
    return 'today';
  }

  return date;
}

function saveToCache(key, value) {
  const cache = platform.getCache('apod', { resource: 'index', scope: 'global', prefix: 'apod'} );
  cache.put(key, JSON.stringify(value), 60 * 60);
}

function handleRequest(req, res, next) {
  const kvm = platform.getKeyValueMap('apod', 'environment');
  kvm.get('NASA_API_KEY', (err, nasaApiKey) => {
    const date = extractDate(req.query.date);
    client.getApodDesc(nasaApiKey, date).then((desc) => {
      const body = { desc };
      res.json(body);
      saveToCache(date, body);
    });
  });
}

function checkCache(req, res, next) {
  const date = extractDate(req.query.date);

  if (date === 'today') {
    next();
    return;
  }

  const cache = platform.getCache('apod', { resource: 'index', scope: 'global', prefix: 'apod'} );
  cache.get(date, (err, data) => {
    if (data !== null) {
      res.send(JSON.parse(data));
    } else {
      next();
    }
  });
}

const index = router.get('/',
  checkCache,
  handleRequest
);

module.exports = index;
