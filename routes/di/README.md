# Implementation using dependency injection

This is an implementation of the APOD proxy built with its dependencies on external configuration and caching injected at start-up.

## Externalized configuration

The NASA api key is injected at startup time via the `nasaApiKey` argument.  In production, `app.js` will fetch the credential from a bound cred-hub service broker instance (or local environment variables), and pass it in to the route.

## Caching

The redis client is injected at startup time via the `redisClient` argument.  In production, `app.js` will construct a redis client via the `redis` library's `createClient` function, with configuration provided by a bound redis service (or local environment variables).

## Testing

Testing is done using `supertest` for making requests, `nock` for stubbing the NASA APOD API, and `redis-mock` for providing an in-memory implementation redis for testing.

As most dependencies are injected, it is very easy to to reconfigure the route to use test configuration / test implementations.
