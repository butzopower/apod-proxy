const express = require('express');
const request = require('supertest');
const index = require('./index');
const nock = require('nock');
const redisMock = require('redis-mock');

const expectedApiKey = 'abc123';

describe('apod desc api: with dependency injection', () => {
  let app;

  beforeEach(() => {
    app = express();
    app.use(index(expectedApiKey, redisMock.createClient()));
  });

  afterEach((done) => {
    nock.cleanAll();
    done();
  });

  it('makes a request to the nasa API', (done) => {
    nock('https://api.nasa.gov')
      .get('/planetary/apod')
      .query({api_key: expectedApiKey})
      .reply(200, {explanation: 'cool-dawg'});


    request(app)
      .get('/')
      .expect(200, {desc: 'cool-dawg'})
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('makes allows a specific date', (done) => {
    nock('https://api.nasa.gov')
      .get('/planetary/apod')
      .query({api_key: expectedApiKey, date: '2017-10-28'})
      .reply(200, {explanation: 'cool-dawg-2017'});

    request(app)
      .get('/?date=2017-10-28')
      .expect(200, {desc: 'cool-dawg-2017'})
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });

  it('caches the response', (done) => {
    // setup nasa API to first 200 then 500 if a future request is made, thus ensuring we hit the cache
    nock('https://api.nasa.gov')
      .get('/planetary/apod')
      .query({api_key: expectedApiKey, date: '2018-10-28'})
      .reply(200, {explanation: 'cool-dawg-2018'})
      .get('/planetary/apod')
      .query({api_key: expectedApiKey, date: '2018-10-28'})
      .reply(500);

    // make the same request twice
    request(app)
      .get('/?date=2018-10-28')
      .expect(200, {desc: 'cool-dawg-2018'})
      .end(function(err, res) {
        if (err) return done(err);

        request(app)
          .get('/?date=2018-10-28')
          .expect(200, {desc: 'cool-dawg-2018'})
          .end(function(err, res) {
            if (err) return done(err);

            done();
          });
      });
  });
});
