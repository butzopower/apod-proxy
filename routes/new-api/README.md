# Implementation using more specific collaborator

This is an implementation of the APOD proxy built using a light-weight wrapper for talking to the platform that implements an API more tailored to the needs of the app.

## Externalized configuration

Externalized configuration is accessed via the `platform`'s `getNasaApiKey` function, which is a simple implementation for the specific requirement of the app. It will fetch the credential from a bound cred-hub service broker instance (or local environment variables).  No vestigial arguments are required.

## Caching

The cache can be via the `platform` object's `cache` property.  The returned cache object is a light-weight wrapper around the redis client, whose configuration is provided by a bound redis service (or local environment variables).

## Testing

Testing is done using `supertest` for making requests, `nock` for stubbing the NASA APOD API, and `mockery` for providing a fake version of the `platform` collaborator during test.

The `platform` mock is slightly simpler than other implementations, as we control its interface and can simplify it to our specific needs / use modern node features like Promises or `async`/`await`.
