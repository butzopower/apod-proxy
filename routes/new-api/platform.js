const services = isCloud() ?
  require('../../services.cloud') :
  require('../../services.local');

const redis = require('redis');
const redisClient = redis.createClient(services.REDIS_OPTIONS);

function isCloud() {
  return process.env.VCAP_SERVICES !== undefined;
}

const cache = {
  put(key, value, ttl) {
    redisClient.setex(key, ttl, value);
  },

  get(key) {
    return new Promise(resolve => {
      redisClient.get(key, (err, data) => resolve(data));
    });
  }
};

const platform = {
  cache,
  getNasaApiKey() {
    return services.NASA_API_KEY;
  }
};

module.exports = platform;
