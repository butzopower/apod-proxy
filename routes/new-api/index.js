const express = require('express');
const client = require('../../services/nasa.client');
const platform = require('./platform');
const router = express.Router();

function extractDate(date) {
  if (date === undefined) {
    return 'today';
  }

  return date;
}

function saveToCache(key, value) {
  platform.cache.store(key, JSON.stringify(value), 60 * 60);
}

async function handleRequest(req, res, next) {
  const nasaApiKey = platform.getNasaApiKey();
  const date = extractDate(req.query.date);
  client.getApodDesc(nasaApiKey, date).then((desc) => {
    const body = { desc };
    res.json(body);
    saveToCache(date, body);
  });
}

async function checkCache(req, res, next) {
  const date = extractDate(req.query.date);

  if (date === 'today') {
    next();
    return;
  }

  const cachedValue = await platform.cache.get(date);

  if (cachedValue !== null) {
    res.send(JSON.parse(cachedValue));
  } else {
    next();
  }
}

const index = router.get('/',
  checkCache,
  handleRequest
);

module.exports = index;
