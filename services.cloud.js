const services = {
    get credentials() {
        return getVCAPServices()['credhub'][0]['credentials'];
    },

    get NASA_API_KEY() {
        return this.credentials['NASA_API_KEY'];
    },

    get REDIS_OPTIONS() {
        const redisCredentials = getVCAPServices()['p.redis'][0]["credentials"];

        const host = redisCredentials["host"];
        const port = redisCredentials["port"];
        const password = redisCredentials["password"];

        return {
            host,
            port,
            password
        }
    }
};

function getVCAPServices() {
    return JSON.parse(process.env.VCAP_SERVICES);
}

module.exports = services;
