const services = {
    get credentials() {
        return process.env;
    },

    get NASA_API_KEY() {
        return this.credentials['NASA_API_KEY'];
    },

    get REDIS_OPTIONS() {
        const host = '127.0.0.1';
        const port = 6379;

        return {
            host,
            port
        }
    }
};

module.exports = services;
