const express = require('express');
const redis = require('redis');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const apigeeRoute = require('./routes/apigee');
const sameApiRoute = require('./routes/same-api');
const newApiRoute = require('./routes/new-api');
const diRoute = require('./routes/di');
const guidRouter = require('./routes/guid');

const services = isCloud() ?
    require('./services.cloud') :
    require('./services.local');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/apigee', apigeeRoute);

app.use('/same-api', sameApiRoute);

app.use('/new-api', newApiRoute);

const redisClient = redis.createClient(services.REDIS_OPTIONS);
app.use('/di', diRoute(services.NASA_API_KEY, redisClient));

if (isCloud()) {
    app.use('/guid', guidRouter);
}

module.exports = app;

function isCloud() {
    return process.env.VCAP_SERVICES !== undefined;
}
